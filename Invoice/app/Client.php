<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    //
    protected $dates = ['deleted_at'];

    public function invoice()
    {
        return $this->hasMany('Invoice');
    }

    public function user()
    {
        return $this->belongsTo('App\Invoice')->withTrashed();
    }
}

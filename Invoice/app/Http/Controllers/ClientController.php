<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all client
        $clients = Client::all();
        
        //load all item to view
        return view('client.index')->with('clients', $clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // load the create form
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation
        $rules = array(
            'business_name'             => 'required|max:50',
            'first_name'                => 'required|max:30',
            'last_name'                 => 'required|max:50',
            'email'                     => 'required|unique:clients|email',
            'address'                   => 'required',
            'city'                      => 'required|max:100',
            'province'                  => 'required|max:100',
            'postal_code'               => 'required|max:5',
            'telephone'                 => 'required|max:20',
            'handphone'                 => 'required|max:20',
            'country_code'              => 'required|max:5',
        );
        $validator = Validator::make($request->all(), $rules);

        // validating
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = Auth::user();

            $client = new Client;
            $client->business_name         = $request->input('business_name');
            $client->first_name            = $request->input('first_name');
            $client->last_name             = $request->input('last_name');
            $client->email                 = $request->input('email');
            $client->address               = $request->input('address');
            $client->city                  = $request->input('city');
            $client->province              = $request->input('province');
            $client->postal_code           = $request->input('postal_code');
            $client->telephone             = $request->input('telephone');
            $client->handphone             = $request->input('handphone');
            $client->country_code          = $request->input('country_code');
            $client->person_id             = $user->id;
            $client->save();

            return redirect('client');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the client
        $client = Client::find($id);
        
        // show the view of selected client
        return view('client.show')->with('clients', $client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the client
        $client = Client::find($id);
        
        // show the edit form and pass the client
        return view('client.edit')->with('clients', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        $rules = array(
            'business_name'             => 'required|max:50',
            'first_name'                => 'required|max:30',
            'last_name'                 => 'required|max:50',
            'email'                     => 'required|unique:clients|email',
            'address'                   => 'required',
            'city'                      => 'required|max:100',
            'province'                  => 'required|max:100',
            'postal_code'               => 'required|max:5',
            'telephone'                 => 'required|max:20',
            'handphone'                 => 'required|max:20',
            'country_code'              => 'required|max:5',
        );
        $validator = Validator::make($request->all(), $rules);

        // validating
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $client = Client::find($id);

            $client->business_name         = $request->input('business_name');
            $client->first_name            = $request->input('first_name');
            $client->last_name             = $request->input('last_name');
            $client->email                 = $request->input('email');
            $client->address               = $request->input('address');
            $client->city                  = $request->input('city');
            $client->province              = $request->input('province');
            $client->postal_code           = $request->input('postal_code');
            $client->telephone             = $request->input('telephone');
            $client->handphone             = $request->input('handphone');
            $client->country_code          = $request->input('country_code');
            $client->save();

            return redirect('client');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $client = Client::find($id);
        $client->delete();

        // redirect
        return redirect('client');
    }
}

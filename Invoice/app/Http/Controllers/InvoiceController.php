<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use App\Item;
use App\Invoice;
use App\InvoiceLine;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use PDF;
use Yajra\Datatables\Datatables;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user->role == 'admin'){
            //get all invoice of a client
            $invoices = Invoice::all();
        } else {
            $invoices = Invoice::where('person_id', $user->id)->get();
        }
        
        //load all invoice to view
        return view('invoice.index')->with('invoices', $invoices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        $items = Item::all();
        // load the create form
        return view('invoice.create')->with(['clients'=> $clients, 'items' => $items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validator
        $rules = array(
            'invoice_date'     => 'required|date',
            'invoice_no'       => 'required|max:50',
            'client_id'        => 'required',
            'total'            => 'required|numeric',
            'in_words'         => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = Auth::user();

            $invoice = new Invoice;
            $invoice->invoice_date  = $request->input('invoice_date');
            $invoice->invoice_no    = $request->input('invoice_no');
            $invoice->total         = $request->input('total');
            $invoice->in_words      = $request->input('in_words');
            $invoice->person_id     = $user->id;

            if($request->input('client_id') == 'new_client') {
                $rules = array(
                    'business_name'             => 'required|max:50',
                    'first_name'                => 'required|max:30',
                    'last_name'                 => 'required|max:50',
                    'email'                     => 'required|unique:clients|email',
                    'address'                   => 'required',
                    'city'                      => 'required|max:100',
                    'province'                  => 'required|max:100',
                    'postal_code'               => 'required|max:5',
                    'telephone'                 => 'required|max:20',
                    'handphone'                 => 'required|max:20',
                    'country_code'              => 'required|max:5',
                );
                $validator = Validator::make($request->all(), $rules);
                // validating
                if($validator->fails()){
                    return redirect()->back()->withErrors($validator)->withInput();
                } else {
                    $user = Auth::user();
                    
                    $client = new Client;
                    $client->business_name         = $request->input('business_name');
                    $client->first_name            = $request->input('first_name');
                    $client->last_name             = $request->input('last_name');
                    $client->email                 = $request->input('email');
                    $client->address               = $request->input('address');
                    $client->city                  = $request->input('city');
                    $client->province              = $request->input('province');
                    $client->postal_code           = $request->input('postal_code');
                    $client->telephone             = $request->input('telephone');
                    $client->handphone             = $request->input('handphone');
                    $client->country_code          = $request->input('country_code');
                    $client->person_id             = $user->id;
                    $client->save();
                    $invoice->client_id            = $client->id;
                }
            } else {
                $invoice->client_id     = $request->input('client_id');
            }

            $rulesLine = array(
                'note.*'        => 'max:100',
                'quantity.*'    => 'required|numeric',
                'item_id.*'     => 'required|numeric',
            );
            $validator = Validator::make($request->all(), $rulesLine);
            $size = count($request->input('quantity'));
            $user = Auth::user();

            if($validator->fails()){
                return redirect()->back()->withErrors(['line' => 'Wrong input for Invoice Line'])->withInput();
            } else {
                $invoice->save();
                for($i = 0 ; $i < $size ; $i++)
                {
                    if($request->input('item_id')[$i] != null && $request->input('quantity')[$i] != null)
                    {
                        $line = new InvoiceLine;

                        $line->item_id = $request->input('item_id')[$i];
                        $line->note = $request->input('note')[$i];
                        $line->quantity = $request->input('quantity')[$i];

                        $line->person_id = $user->id;
                        $line->invoice_id = $invoice->id;

                        $line->save();
                    }
                }
            }
            return redirect('invoice');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the invoice
        $invoice = Invoice::find($id);
        if($invoice == null){
            return redirect('invoice');
        }

        // get all the invoice line
        $lines = InvoiceLine::where('invoice_id',$id)->get();
        
        // show the view of selected item
        return view('invoice.show')->with(['invoices'=> $invoice, 'invoiceLines'=> $lines]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get all the client
        $clients = Client::all();
        // get the invoice
        $invoice = Invoice::find($id);
        if($invoice == null){
            return redirect('invoice');
        }
        
        // get all the invoice line
        $lines = InvoiceLine::where('invoice_id',$id)->get();

        // get all items
        $items = Item::all();

        // show the edit form and pass the nerd
        return view('invoice.edit')->with(['invoices'=> $invoice, 'clients'=> $clients, 'invoiceLines' => $lines, 'items' => $items]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'invoice_date'     => 'required|date',
            'invoice_no'       => 'required|max:50',
            'client_id'        => 'required',
            'total'            => 'required|numeric',
            'in_words'         => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $invoice = Invoice::find($id);

            $invoice->invoice_date  = $request->input('invoice_date');
            $invoice->invoice_no    = $request->input('invoice_no');
            $invoice->total         = $request->input('total');
            $invoice->in_words      = $request->input('in_words');

            if($request->input('client_id') == 'new_client') {
                $rules = array(
                    'business_name'             => 'required|max:50',
                    'first_name'                => 'required|max:30',
                    'last_name'                 => 'required|max:50',
                    'email'                     => 'required|unique:clients|email',
                    'address'                   => 'required',
                    'city'                      => 'required|max:100',
                    'province'                  => 'required|max:100',
                    'postal_code'               => 'required|max:5',
                    'telephone'                 => 'required|max:20',
                    'handphone'                 => 'required|max:20',
                    'country_code'              => 'required|max:5',
                );
                $validator = Validator::make($request->all(), $rules);
                // validating
                if($validator->fails()){
                    return redirect()->back()->withErrors($validator)->withInput();
                } else {
                    $user = Auth::user();
                    
                    $client = new Client;
                    $client->business_name         = $request->input('business_name');
                    $client->first_name            = $request->input('first_name');
                    $client->last_name             = $request->input('last_name');
                    $client->email                 = $request->input('email');
                    $client->address               = $request->input('address');
                    $client->city                  = $request->input('city');
                    $client->province              = $request->input('province');
                    $client->postal_code           = $request->input('postal_code');
                    $client->telephone             = $request->input('telephone');
                    $client->handphone             = $request->input('handphone');
                    $client->country_code          = $request->input('country_code');
                    $client->person_id             = $user->id;
                    $client->save();
                    $invoice->client_id            = $client->id;
                }
            } else {
                $invoice->client_id     = $request->input('client_id');
            }

            $invoice->save();

            $rulesLine = array(
                'note.*'        => 'max:100',
                'quantity.*'    => 'required|numeric',
                'item_id.*'     => 'required|numeric',
            );
            $validator = Validator::make($request->all(), $rulesLine);

            if($validator->fails()){
                return redirect()->back()->withErrors(['line' => 'Wrong input for Invoice Line'])->withInput();
            } else {
                $user = Auth::user();
                // updated line count
                $size;
                if(count($request->input('line_id')) > 0){
                    $size = count($request->input('line_id'));
                } else {
                    $size = 0;
                }

                // delete other line
                if($size > 0){
                    InvoiceLine::where('invoice_id', $id)->whereNotIn('id', $request->input('line_id'))->delete();   
                } else if($size <= 0){
                    InvoiceLine::where('invoice_id', $id)->delete();
                }

                // updating line
                for($i = 0 ; $i < $size ; $i++)
                {
                    $line = InvoiceLine::where('id', $request->input('line_id')[$i])->first();

                    $line->item_id = $request->input('item_id')[$i];
                    $line->note = $request->input('note')[$i];
                    $line->quantity = $request->input('quantity')[$i];

                    $line->save();
                }

                $theRest = count($request->input('quantity'));

                // creating new line
                for($i = $size ; $i < $theRest ; $i++)
                {
                    $line = new InvoiceLine;
                    
                    $line->item_id = $request->input('item_id')[$i];
                    $line->note = $request->input('note')[$i];
                    $line->quantity = $request->input('quantity')[$i];

                    $line->person_id = $user->id;
                    $line->invoice_id = $id;

                    $line->save();
                }
            }

            return redirect('invoice');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $invoice = Invoice::find($id);
        if($invoice == null){
            return redirect('invoice');
        }
        $invoice->delete();

        // redirect
        return redirect('invoice');
    }

    public function viewPDF(Request $request, $id)
    {
        $invoice = Invoice::find($id);
        $lines = InvoiceLine::where('invoice_id',$id)->get();
        $pdf = PDF::loadView('printpreview',['invoices'=> $invoice, 'invoiceLines'=> $lines]);
        return $pdf->stream('printpreview.pdf');
        // return view('printpreview')->with(['invoices'=> $invoice, 'invoiceLines'=> $lines]);
    }
}

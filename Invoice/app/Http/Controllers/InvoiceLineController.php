<?php

namespace App\Http\Controllers;

use App\User;
use App\Item;
use App\Invoice;
use App\InvoiceLine;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class InvoiceLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $invoice = Invoice::find($id);
        $items = Item::all();
        if($invoice == null){
            return redirect('invoice');
        }
        // load the create form
        return view('invoice.line.create')->with(['invoices' => $invoice, 'items' => $items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // validator
        $rulesLine = array(
            'note'          => 'array',
            'note.*'        => 'max:100',
            'quantity'      => 'array',
            'quantity.*'    => 'required|numeric',
            'item_id'       => 'array',
            'item_id.*'     => 'required|numeric',
        );
        $validator = Validator::make($request->all(), $rulesLine);
        $size = count($request->input('item_id'));

        // if($validator->fails()){
        //     return redirect()->back()->withErrors($validator)->withInput();
        // } else {
        //     $user = Auth::user();

        //     $line = new InvoiceLine();
        //     $line->note         = $request->input('note');
        //     $line->quantity     = $request->input('quantity');
        //     $line->item_id      = $request->input('item_id');

        //     $line->person_id    = $user->id;
        //     $line->invoice_id   = $id;

        //     $line->save();
        // }

        if($validator->fails() && $size > 0){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = Auth::user();
            for($i = 0 ; $i < $size ; $i++)
            {
                if($request->input('item_id')[$i] != null)
                {
                    $line = new InvoiceLine();
                    $line->item_id = $request->input('item_id')[$i];
                    $line->note = $request->input('note')[$i];
                    $line->quantity = $request->input('quantity')[$i];
        
                    $line->person_id    = $user->id;
                    $line->invoice_id   = $id;
        
                    $line->save();
                }
            }
        }

        // get the invoice
        $invoice = Invoice::find($id);
    
        // get all the invoice line
        $lines = InvoiceLine::where('invoice_id',$id)->get();

        return redirect('invoice/'.$id.'/edit')->with(['invoices'=> $invoice, 'invoiceLines'=> $lines]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $line_id)
    {
        // get all the client
        $items = Item::withTrashed()->get();
        // get all the invoice
        $invoice = Invoice::find($id);
        // get the line
        $line = InvoiceLine::find($line_id);

        if($invoice == null){
            return redirect('invoice/'.$id);
        }
        if($line == null){
            return redirect('invoice/'.$id);
        }
        
        // show the edit form and pass the nerd
        return view('invoice.line.edit')->with(['invoices'=>$invoice, 'lines'=> $line, 'items'=> $items]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $line_id)
    {
        // validator
        $rules = array(
            'note'          => 'max:100',
            'quantity'      => 'required|numeric',
            'item_id'       => 'required|numeric',
        );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = Auth::user();

            $line = InvoiceLine::find($line_id);
            $line->note         = $request->input('note');
            $line->quantity     = $request->input('quantity');
            $line->item_id      = $request->input('item_id');


            $line->save();
            // get the invoice
            $invoice = Invoice::find($id);
        
            // get all the invoice line
            $lines = InvoiceLine::where('invoice_id',$id)->get();

            return redirect('invoice/'.$id.'/edit')->with(['invoices'=> $invoice, 'invoiceLines'=> $lines]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $line_id)
    {
        // delete
        $line = InvoiceLine::find($line_id);
        if($line == null){
            return redirect('invoice/'.$id);
        }
        $line->delete();

        // get the invoice
        $invoice = Invoice::find($id);
        
        // get all the invoice line
        $lines = InvoiceLine::where('invoice_id',$id)->get();

        if($invoice == null){
            return redirect('invoice/'.$id);
        }
        
        // show the view of selected item
        return redirect('invoice/'.$id.'/edit')->with(['invoices'=> $invoice, 'invoiceLines'=> $lines]);
    }
}
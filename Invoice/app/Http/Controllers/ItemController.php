<?php

namespace App\Http\Controllers;

use App\User;
use App\Item;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all item
        $items = Item::all();
        
        //load all item to view
        return view('item.index')->with('items', $items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // load the create form
        return view('item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'             => 'required|max:150',
            'description'      => '',
            'item_code'        => 'required|max:25',
            'price'            => 'required|numeric',
        );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = Auth::user();

            $item = new Item;
            $item->name         = $request->input('name');
            $item->description  = $request->input('description');
            $item->item_code    = $request->input('item_code');
            $item->price        = $request->input('price');
            $item->person_id    = $user->id;
            $item->save();

            return redirect('item');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the item
        $item = Item::find($id);
        if($item == null){
            return redirect('item'); 
        }
        
        // show the view of selected item
        return view('item.show')->with('items', $item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the item
        $item = Item::find($id);

        if($item == null){
            return redirect('item'); 
        }
        
        // show the edit form and pass the nerd
        return view('item.edit')->with('items', $item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'             => 'required|max:150',
            'description'      => '',
            'item_code'        => 'required|max:25',
            'price'            => 'required|numeric',
        );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            // Session::flash('failed', 'Wrong input.');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $item = Item::find($id);

            $item->name         = $request->input('name');
            $item->description  = $request->input('description');
            $item->item_code    = $request->input('item_code');
            $item->price        = $request->input('price');

            $item->save();

            // Session flash = notify if success or error
            // Session::flash('success', 'Item has been updated');


            // Redirect to all item
            return redirect('item');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $item = Item::find($id);
        if($item == null){
            return redirect('item'); 
        }
        $item->delete();

        // redirect
        return redirect('item');
    }
}

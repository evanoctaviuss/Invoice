<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    public function index()
    {
        //get all client
        $users = User::all();
        
        //load all item to view
        return view('user.index')->with('users', $users);
    }

    public function show($id)
    {
        // get the client
        $user = User::find($id);
        
        // show the view of selected client
        return view('user.show')->with('users', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the client
        $user = User::find($id);
        
        // show the edit form and pass the client
        return view('user.edit')->with('users', $user);
    }

    public function update(Request $request, $id) {
        // validation
        $rules = array(
            'name'  => 'required',
            'email' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = User::find($id);

            $user->name     = $request->input('name');
            $user->email    = $request->input('email');
            $user->save();

            if($request->input('old_password') != null || $request->input('new_password') != null || $request->input('conf_password') != null){
                $rules = array(
                    'old_password'  => 'required|min:6',
                    'new_password'  => 'required|same:conf_password|min:6',
                    'conf_password' => 'required|same:new_password|min:6',
                );
        
                $validator = Validator::make($request->all(), $rules);
                
                if($validator->fails()){
                    return redirect()->back()->withErrors($validator)->withInput();
                } else {
                    $user = User::find($id);
        
                    $oldPassword = $request->input('old_password');
                    $newPassword = $request->input('new_password');
        
                    if(Hash::check($oldPassword, $user->password)){
                        $user->password = bcrypt($newPassword);
        
                        $user->save();
                        return redirect('user');
                    } else {
                        return redirect()->back()->withErrors(['pass' => 'Wrong Old Password']);
                    }
                }
            }

            return redirect('user');
        }
    }

    public function destroy($id)
    {
        // delete
        $user = User::find($id);
        if($user == null){
            return redirect('user');
        } else if($user->role == 'admin'){
            return redirect('user');
        } else {
            $user->delete();
        }

        // redirect
        return redirect('user');
    }

    public function change(){

        return view('user.changepassword');
    }

    public function check(Request $request, $id){
        $rules = array(
            'old_password'  => 'required|min:6',
            'new_password'  => 'required|same:conf_password|min:6',
            'conf_password' => 'required|same:new_password|min:6',
        );

        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = User::find($id);

            $oldPassword = $request->input('old_password');
            $newPassword = $request->input('new_password');

            if(Hash::check($oldPassword, $user->password)){
                $user->password = bcrypt($newPassword);

                $user->save();
                return redirect('invoice');
            } else {
                return redirect()->back()->withErrors(['pass' => 'Wrong Old Password']);
            }
        }
    }
}
<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuthority
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if($user->role != 'admin'){
            return redirect('invoice');
        }
        
        return $next($request);
    }
}
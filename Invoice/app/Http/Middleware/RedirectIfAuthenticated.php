<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('invoice');
        }

        return $next($request);
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}

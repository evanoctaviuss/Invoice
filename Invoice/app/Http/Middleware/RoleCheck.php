<?php

namespace App\Http\Middleware;

use Closure;
use App\Invoice;

class RoleCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        $id = $request->route()->parameter('id');
        $invoice = Invoice::where('id',$id)->first();
        if($invoice == null){
            return redirect('invoice');
        } else {
            if($user->id != $invoice->person_id){
                if($user->role != 'admin'){
                    return redirect('invoice');
                }
            }
        }
        return $next($request);       
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    //
    protected $dates = ['deleted_at'];

    /**
   * Override parent boot and Call deleting event
   *
   * @return void
   */
   protected static function boot() 
   {
        parent::boot();

        static::deleting(function($invoices) {
            foreach ($invoices->invoiceLine()->get() as $lines) {
                $lines->delete();
            }
        });
   }

    public function client()
    {
        return $this->belongsTo('App\Client')->withTrashed();
    }

    public function invoiceLine()
    {
        return $this->hasMany('App\InvoiceLine');
    }

    public function user()
    {
        return $this->belongsTo('App\Invoice')->withTrashed();
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
    //
    protected $dates = ['deleted_at'];

    public function invoiceLine()
    {
        return $this->hasMany('App\InvoiceLine');
    }

    public function user()
    {
        return $this->belongsTo('App\Invoice')->withTrashed();
    }
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name', 50);
            $table->string('first_name', 30);
            $table->string('last_name', 50);
            $table->string('email',191)->unique();
            $table->text('address');
            $table->string('city', 100);
            $table->string('province', 100);
            $table->char('postal_code', 5);
            $table->string('telephone', 20);
            $table->string('handphone', 20);
            $table->string('country_code', 5);
            $table->integer('person_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('person_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}

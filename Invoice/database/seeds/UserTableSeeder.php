<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $role_admin = Role::where('role', 'admin')->first();
        $admin = new User();
        $admin->name = 'admin';
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('secret');
        $admin->last_login = Carbon\Carbon::now();
        $admin->role = 'admin';
        $admin->save();
        // $admin->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'user';
        $user->email = 'user@example.com';
        $user->password = bcrypt('userpas');
        $user->last_login = Carbon\Carbon::now();
        $user->role = 'user';
        $user->save();
    }
}

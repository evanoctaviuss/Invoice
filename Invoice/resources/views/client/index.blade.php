@extends('layouts.app')

@section('content')

<div style="width:auto; height:100%;" class="container" id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Showing all Client</h1>
    </div>
</div>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="display responsive no-wrap table table-striped table-bordered" id="client_table" width="100%">
    <thead>
        <tr>
            <th>Business Name</th>
            <th>E-mail</th>
            <th>Telephone</th>
            <th>Handphone</th>
            <th>Option</th>
        </tr>
    </thead>
    <tbody>
    @foreach($clients as $key => $value)
        <tr>
            <td>{{ $value->business_name }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->telephone }}</td>
            <td>{{ $value->handphone }}</td>


            <td>
            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
            <a class="btn btn-small btn-success" href="{{ URL::to('client/' . $value->id) }}"><span class="glyphicon glyphicon-fullscreen"></span></a>

            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
            <a class="btn btn-small btn-info" href="{{ URL::to('client/' . $value->id . '/edit') }}"><span class="glyphicon glyphicon-edit"></span></a>
            
            <!-- delete the invoice (uses the destroy method DESTROY /invoice/{id} -->
            <!-- we will add this later since its a little more complicated than the other two buttons -->
            <form class="del" method="POST" action="{{ URL::to('client/'.$value->id) }}" style="display:none;">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn btn-danger">
                    <span class="glyphicon glyphicon-remove-sign"></span>
                </button>
            </form>
        </td>
        </tr>
    @endforeach
    </tbody>
</table>

<nav class="navbar navbar-inverse pull-right" style="margin-top:20px;">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('client/create') }}"><span class="glyphicon glyphicon-plus"></span> Create a client</a>
    </ul>
</nav>

</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#client_table').DataTable({
            responsive: true
        });
    });
    window.onload = function() {
        var del = document.getElementsByClassName('del');
        if("{{ Auth::user()->role }}" == "admin") {
            for(index = 0 ; index < del.length ; ++index){
                del[index].style.display = 'inline-block';
            } 
        } else {
            for(index = 0 ; index < del.length ; ++index){
                del[index].style.display = 'none';
            } 
        }
    }
    $(".del").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
</script>
@endsection
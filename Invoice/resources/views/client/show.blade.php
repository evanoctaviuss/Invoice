@extends('layouts.app')

@section('content')

<div style="width:auto; height:100%;" class="container" id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Showing {{ $clients->name }}</h1>
        </div>
    </div>

    <div class="jumbotron" style="display:table; width:100%;" >
        <table cellpadding="5" cellspacing="5" width="100%">
            <tr>
                <td><strong>Business Name   </strong></td>     
                <td>:</td>    
                <td>{{ $clients->business_name }}</td>
            </tr>     
            <tr>         
                <td><strong>First Name      </strong></td>
                <td>:</td>
                <td>{{ $clients->first_name }}</td>
            </tr>    
            <tr>         
                <td><strong>Last Name       </strong></td> 
                <td>:</td>
                <td>{{ $clients->last_name }}</td>     
            </tr>
            <tr>         
                <td><strong>E-mail          </strong></td>
                <td>:</td>
                <td>{{ $clients->email }}</td>     
            </tr>
            <tr>         
                <td><strong>Address         </strong></td>
                <td>:</td>
                <td>{{ $clients->address }}</td>     
            </tr>
            <tr>         
                <td><strong>City            </strong></td>
                <td>:</td>
                <td>{{ $clients->city }}</td>     
            </tr>
            <tr>         
                <td><strong>Province        </strong></td>
                <td>:</td>
                <td>{{ $clients->province }}</td>     
            </tr>
            <tr>         
                <td><strong>Postal Code     </strong></td>
                <td>:</td>
                <td>{{ $clients->postal_code }}</td>     
            </tr>
            <tr>         
                <td><strong>Telephone       </strong></td>
                <td>:</td>
                <td>{{ $clients->telephone }}</td>     
            </tr>
            <tr>         
                <td><strong>Handphone       </strong></td>
                <td>:</td>
                <td>{{ $clients->handphone }}</td>     
            </tr>
            <tr>         
                <td><strong>Country Code    </strong></td>
                <td>:</td>
                <td>{{ $clients->country_code }}</td>     
            </tr>
        </table>
    </div>

    <div class="pull-right">
        <a href="{{ URL::to('client') }}" class="btn btn-primary">
            Back
        </a>
    </div>

</div>
@endsection
@extends('layouts.app')

@section('content')

<div style="width:auto; height:100%;" class="container" id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Form</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Invoice</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ URL::to('invoice/'.$invoices->id) }}" id="form">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group{{ $errors->has('invoice_date') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Invoice Date</label>

                            <div class="col-md-6">
                                <input id="invoice_date" type="date" class="form-control" name="invoice_date" value="{{ $invoices->invoice_date }}" required autofocus>

                                @if ($errors->has('invoice_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('invoice_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('invoice_no') ? ' has-error' : '' }}">
                            <label for="invoice_no" class="col-md-4 control-label">Invoice Number</label>

                            <div class="col-md-6">
                                <input id="invoice_no" type="number" class="form-control" name="invoice_no" value="{{ $invoices->invoice_no }}" required>

                                @if ($errors->has('invoice_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('invoice_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('total') ? ' has-error' : '' }}">
                            <label for="total" class="col-md-4 control-label">Total</label>

                            <div class="col-md-6">
                                <input id="total" type="number" class="form-control" name="total" value="{{ $invoices->total }}" required>

                                @if ($errors->has('total'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('total') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('in_words') ? ' has-error' : '' }}">
                            <label for="in_words" class="col-md-4 control-label">Said</label>

                            <div class="col-md-6">
                                <input id="invoice_no" type="text" class="form-control" name="in_words" value="{{ $invoices->in_words }}" required>

                                @if ($errors->has('in_words'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('in_words') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<!-- Selece or register client-->
<div class="panel-body">
	<div class="form-group{{ $errors->has('client_id') ? ' has-error' : '' }}">
        @if ($errors->has('client_id'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('client_id') }}</strong>
            </span>
        @endif
        @if ($errors->has('business_name'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('business_name') }}</strong>
            </span>
        @endif
        @if ($errors->has('first_name'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('first_name') }}</strong>
            </span>
        @endif
        @if ($errors->has('last_name'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('last_name') }}</strong>
            </span>
        @endif
        @if ($errors->has('email'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('email') }}</strong>
            </span>
        @endif
        @if ($errors->has('address'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('address') }}</strong>
            </span>
        @endif
        @if ($errors->has('city'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('city') }}</strong>
            </span>
        @endif
        @if ($errors->has('province'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('province') }}</strong>
            </span>
        @endif
        @if ($errors->has('postal_code'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('postal_code') }}</strong>
            </span>
        @endif
        @if ($errors->has('telephone'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('telephone') }}</strong>
            </span>
        @endif
        @if ($errors->has('handphone'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('handphone') }}</strong>
            </span>
        @endif
        @if ($errors->has('country_code'))
            <span class="help-block">
                <strong style="color:#a94442;">{{ $errors->first('country_code') }}</strong>
            </span>
        @endif
		<label for="client_id" class="col-md-4 control-label">Business Name</label>

		<div class="col-md-6">
			<select id="client_id" list="clients" class="form-control" name="client_id" value="{{ $invoices->client_id }}">

            <datalist id="clients">
                <option value="" disabled selected> Choose or create new client </option>
                @foreach($clients as $key => $value)
                    @if ($invoices->client_id == $value->id)
                        <option value="{{ $value->id }}" label="" selected>{{ $value->business_name }}</option>
                    @else
                        <option value="{{ $value->id }}" label="">{{ $value->business_name }}</option>
                    @endif
                @endforeach
                <option value="new_client">Create New Client</option>>
            </datalist>
            </select>
		</div>
	</div>
</div>

<!-- Form for new client registration -->
<div id="new_client" class="register" style="display:none">
    <div class="panel-body">
        <div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">
            <label for="business_name" class="col-md-4 control-label">Business Name</label>

            <div class="col-md-6">
                <input id="business_name" type="text" class="form-control" name="business_name" value="{{ old('business_name') }}" >

                @if ($errors->has('business_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('business_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            <label for="first_name" class="col-md-4 control-label">First Name</label>

            <div class="col-md-6">
                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" >

                @if ($errors->has('first_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
            <label for="last_name" class="col-md-4 control-label">Last Name</label>

            <div class="col-md-6">
                <input id="last_name" type="text" class="form-control" name="last_name"  value="{{ old('last_name') }}" >

                @if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">E-mail</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email"  value="{{ old('email') }}" >

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
            <label for="address" class="col-md-4 control-label">Address</label>

            <div class="col-md-6">
                <input id="address" type="text" class="form-control" name="address"  value="{{ old('address') }}" >

                @if ($errors->has('address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
            <label for="city" class="col-md-4 control-label">City</label>

            <div class="col-md-6">
                <input id="city" type="text" class="form-control" name="city"  value="{{ old('city') }}" >

                @if ($errors->has('city'))
                    <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
            <label for="province" class="col-md-4 control-label">Province</label>

            <div class="col-md-6">
                <input id="province" type="text" class="form-control" name="province"  value="{{ old('province') }}" >

                @if ($errors->has('province'))
                    <span class="help-block">
                        <strong>{{ $errors->first('province') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
            <label for="postal_code" class="col-md-4 control-label">Postal Code</label>

            <div class="col-md-6">
                <input id="postal_code" type="text" class="form-control" name="postal_code"  value="{{ old('postal_code') }}" >

                @if ($errors->has('postal_code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('postal_code') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
            <label for="telephone" class="col-md-4 control-label">Telephone</label>

            <div class="col-md-6">
                <input id="telephone" type="text" class="form-control" name="telephone"  value="{{ old('telephone') }}" >

                @if ($errors->has('telephone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('telephone') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('handphone') ? ' has-error' : '' }}">
            <label for="handphone" class="col-md-4 control-label">Handphone</label>

            <div class="col-md-6">
                <input id="handphone" type="text" class="form-control" name="handphone"  value="{{ old('handphone') }}" >

                @if ($errors->has('handphone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('handphone') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('country_code') ? ' has-error' : '' }}">
            <label for="country_code" class="col-md-4 control-label">Country Code</label>

            <div class="col-md-6">
                <input id="country_code" type="text" class="form-control" name="country_code"  value="{{ old('country_code') }}" >

                @if ($errors->has('country_code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('country_code') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- end -->

<!-- Invoice Line-->
<div class="panel-heading text-center">Invoice Lines</div>
    @if($errors->has('line'))
        <span class="help-block">
            <strong style="color:#a94442;">{{ $errors->first('line') }}</strong>
        </span>
    @endif

<div>
    <table class="display responsive no-wrap table table-striped table-bordered" width="100%">
        <thead>
            <tr>
                <th>Item</th>
                <th>Quantity</th>
                <th>Note</th>
                <th>Option</th>
            </tr>
        </thead>
        <tbody id="lines">
        <!-- List of item -->
        @foreach($invoiceLines as $keys => $values)
            <tr id="line">
                <td style="display: none;">
                    <div >
                        <input id="line_id" type="number" class="form-control" name="line_id[]" value="{{ $values->id }}">
                    </div>
                </td>
                <td>
                    <div>
            
                        <div class="col-md-10">
                            <select id="item_id" list="items" class="form-control" name="item_id[]" value="{{ old('item_id[]') }}">

                            <datalist id="items">
                                @foreach($items as $key => $value)
                                    @if($values->id == $value->id)
                                        <option value="{{ $value->id }}" label="" selected>{{ $value->name }}</option>
                                    @else
                                        <option value="{{ $value->id }}" label="">{{ $value->name }}</option>
                                    @endif
                                @endforeach
                            </datalist>
                            </select>
                        </div>
                    </div>
                </td>
                <!-- end -->

                <td>
                    <div >

                        <div class="col-md-10">
                            <input id="quantity" type="text" class="form-control" name="quantity[]" value="{{ $values->quantity }}">

                        </div>
                    </div>
                </td>

                <td>
                    <div>

                        <div class="col-md-10">
                            <input id="note" type="text" class="form-control" name="note[]"  value="{{ $values->note }}" >

                        </div>
                    </div>
                </td>
                <td>
                    <input class="button-remove" type="button" value="Delete box">
                </td>
            </tr>
            @endforeach
            <tr class="clone" id="line" style="display:none;">
                <td>
                    <div>
            
                        <div class="col-md-10">
                            <select id="item_id" list="items" class="form-control" name="item_id[]" value="{{ old('item_id[]') }}" disabled="disabled">
                            <datalist id="items">
                                <option value="" disabled selected> Choose item </option>
                                @foreach($items as $key => $value)
                                <option value="{{ $value->id }}" label="">{{ $value->name }}</option>
                                @endforeach
                            </datalist>
                            </select>
                        </div>
                    </div>
                </td>
                <!-- end -->

                <td>
                    <div >

                        <div class="col-md-10">
                            <input id="quantity" type="text" class="form-control" name="quantity[]" value="{{ old('quantity[]') }}" disabled="disabled">

                        </div>
                    </div>
                </td>

                <td>
                    <div>

                        <div class="col-md-10">
                            <input id="note" type="text" class="form-control" name="note[]"  value="{{ old('note[]') }}" disabled="disabled">

                        </div>
                    </div>
                </td>
                <td>
                <a class="button-remove btn btn-primary" type="button"><span class="glyphicon glyphicon-plus"></span> Delete Row</a>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="col-md-2 pull-right">
        <a id="clone" class="btn btn-primary">
            <span class="glyphicon glyphicon-plus"></span>
            Add New Line
        </a>
    </div>
</div>
<!-- End -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-5">
                                <button type="submit" form="form" class="btn btn-primary">
                                    Register
                                </button>
                                <a href="{{ URL::to('invoice') }}" class="btn btn-primary">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$(function() {
    $('#client_id').change(function(){
        $('.register').hide();
        $('#' + $(this).val()).show();
        
        if($('.register').is(':visible')){
            $('input[name="business_name"]').prop('required',true);
            $('input[name="first_name"]').prop('required',true);
            $('input[name="last_name"]').prop('required',true);
            $('input[name="email"]').prop('required',true);
            $('input[name="address"]').prop('required',true);
            $('input[name="city"]').prop('required',true);
            $('input[name="province"]').prop('required',true);
            $('input[name="postal_code"]').prop('required',true);
            $('input[name="telephone"]').prop('required',true);
            $('input[name="handphone"]').prop('required',true);
            $('input[name="country_code"]').prop('required',true);
        } else {
            $('input[name="business_name"]').prop('required',false);
            $('input[name="first_name"]').prop('required',false);
            $('input[name="last_name"]').prop('required',false);
            $('input[name="email"]').prop('required',false);
            $('input[name="address"]').prop('required',false);
            $('input[name="city"]').prop('required',false);
            $('input[name="province"]').prop('required',false);
            $('input[name="postal_code"]').prop('required',false);
            $('input[name="telephone"]').prop('required',false);
            $('input[name="handphone"]').prop('required',false);
            $('input[name="country_code"]').prop('required',false);
        }
    });
});

$('#clone').click(function(e) {
    e.preventDefault();
    var $clones = $('.clone:first').clone().appendTo('#lines').show();
    $clones.find("input, select").prop("disabled",false);
    $clones.find("select[id=item_id], input[id=quantity]").prop('required',true).find("input").val("");
});

$(document).on("click", ".button-remove", function() {
    $(this).closest("#line").remove();
});
</script>
@endsection
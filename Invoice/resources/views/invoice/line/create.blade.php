@extends('layouts.app')

@section('content')

<div class="container">

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register new lines</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ URL::to('invoice/'.$invoices->id.'/line') }}">
                        {{ csrf_field() }}

                        <!-- List of item -->
                        <div class="form-group{{ $errors->has('item_id') ? ' has-error' : '' }}">
                            <label for="item_id" class="col-md-4 control-label">Item</label>
                    
                            <div class="col-md-6">
                                <select id="item_id" list="items" class="form-control" name="item_id" value="{{ old('item_id') }}">
                    
                                @if ($errors->has('item_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('item_id') }}</strong>
                                    </span>
                                @endif

                                <datalist id="items">
                                    <option value="" disabled selected> Choose item </option>
                                    @foreach($items as $key => $value)
                                    <option value="{{ $value->id }}" label="">{{ $value->name }}</option>
                                    @endforeach
                                </datalist>
                                </select>
                            </div>
                        </div>
                        <!-- end -->

                        <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                            <label for="note" class="col-md-4 control-label">Note</label>

                            <div class="col-md-6">
                                <input id="note" type="text" class="form-control" name="note"  value="{{ old('note') }}" >

                                @if ($errors->has('note'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                            <label for="quantity" class="col-md-4 control-label">Quantity</label>

                            <div class="col-md-6">
                                <input id="quantity" type="number" class="form-control" name="quantity"  value="{{ old('quantity') }}" required>

                                @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection

@section('script')
<script type="text/javascript">
    var item_list = @json($items);
</script>
@endsection
@extends('layouts.app')

@section('content')

<div class="container">

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register new lines</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ URL::to('invoice/'.$invoices->id.'/line/'.$lines->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <!-- List of item -->
                        <div class="form-group{{ $errors->has('item_id') ? ' has-error' : '' }}">
                            <label for="item_id" class="col-md-4 control-label">Item</label>
                    
                            <div class="col-md-6">
                                <select id="item_id" list="items" class="form-control" name="item_id" value="{{ old('item_id') }}">
                    
                                @if ($errors->has('item_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('item_id') }}</strong>
                                    </span>
                                @endif

                                <datalist id="items">
                                    <option value="" disabled> Choose item </option>
                                    @foreach($items as $key => $value)
                                        @if( $value->id == $lines->item_id )
                                            <option value="{{ $lines->item_id }}" label="" selected>{{ $lines->item->name }}</option>
                                        @elseif( $value->deleted_at == null )
                                            <option value="{{ $value->id }}" label="">{{ $value->name }}</option>
                                        @endif
                                    @endforeach
                                </datalist>
                                </select>
                            </div>
                        </div>
                        <!-- end -->

                        <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                            <label for="note" class="col-md-4 control-label">Note</label>

                            <div class="col-md-6">
                                <input id="note" type="text" class="form-control" name="note"  value="{{ $lines->note }}" >

                                @if ($errors->has('note'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                            <label for="quantity" class="col-md-4 control-label">Quantity</label>

                            <div class="col-md-6">
                                <input id="quantity" type="number" class="form-control" name="quantity"  value="{{ $lines->quantity }}" required>

                                @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                                <a href="{{ URL::to('invoice/'.$invoices->id) }}" class="btn btn-primary">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection

@section('script')
<script type="text/javascript">

var item_list = @json($items);
$(function() {
    $('#item_id').change(function(){
        for(var i = 0 ; i < item_list.length ; i++){
            if(item_list[i]['id'] == $('#item_id').val()){
                $('#subtotal').val(item_list[i]['price'] * $('#quantity').val());
            }
        }
    });
});
$(function() {
    $('#quantity').change(function(){
        for(var i = 0 ; i < item_list.length ; i++){
            if(item_list[i]['id'] == $('#item_id').val()){
                $('#subtotal').val(item_list[i]['price'] * $('#quantity').val());
            }
        }
    });
});
</script>
@endsection
@extends('layouts.app')

@section('content')

<div style="width:auto; height:100%;" class="container" id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Showing Invoice</h1>
        </div>
    </div>

    <div class="jumbotron">
        <table cellpadding="5" cellspacing="5" width="100%">
            <tr>
                <td><strong>Date</strong></td>     
                <td>:</td>    
                <td>{{ date('d-m-Y', strtotime($invoices->invoice_date)) }}</td>
            </tr>     
            <tr>         
                <td><strong>Invoice Number</strong></td>
                <td>:</td>
                <td>{{ $invoices->invoice_no }}</td>
            </tr>    
            <tr>         
                <td><strong>Client Name</strong></td> 
                <td>:</td>
                <td>{{ $invoices->client->business_name }}</td>     
            </tr>
        </table>
        <div class="text-center">
            <a class="btn btn-small btn-success" href="{{ URL::to('invoice/'.$invoices->id.'/pdf') }}">PDF preview</a>
        </div>
    </div>

    <h1>Invoice Line</h1>
    <table class="display responsive no-wrap table table-striped table-bordered" id="lines" width="100%">
        <thead>
            <tr>
                <th>Item</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>

        @foreach($invoiceLines as $key => $value)
            <tr>
                <td><a href="{{ URL::to('item/' . $value->item_id) }}">{{ $value->item->name }}</a></td>
                <td>{{ $value->note }}</td>
                <td>{{ $value->quantity }}</td>
                <td>{{ $value->item->price }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <strong>Total       :</strong>{{ $invoices->total }}<br>
    <strong>Said     :</strong>{{ $invoices->in_words }}<br>

    <div class="pull-right">
        <a href="{{ URL::to('invoice') }}" class="btn btn-primary">
            Back
        </a>
    </div>

</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#lines').DataTable();
    });
    $(".del").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
</script>
@endsection
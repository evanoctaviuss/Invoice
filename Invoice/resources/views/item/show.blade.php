@extends('layouts.app')

@section('content')

<div style="width:auto; height:100%;" class="container" id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Showing {{ $items->name }}</h1>
    </div>
</div>

    <div class="jumbotron">
        <table cellpadding="5" cellspacing="5" width="100%">    
            <tr>         
                <td><strong>Description     </strong></td>
                <td>:</td>
                <td>{{ $items->description }}</td>
            </tr>    
            <tr>         
                <td><strong>Item Code       </strong></td> 
                <td>:</td>
                <td>{{ $items->item_code }}</td>     
            </tr>
            <tr>         
                <td><strong>Price       </strong></td> 
                <td>:</td>
                <td>{{ $items->price }}</td>     
            </tr>
        </table>
    </div>
    <div class="pull-right">
        <a href="{{ URL::to('item') }}" class="btn btn-primary">
            Back
        </a>
    </div>

</div>
@endsection
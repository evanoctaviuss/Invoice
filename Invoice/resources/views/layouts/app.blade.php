<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Invoice') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}" type="text/css">
    <!-- <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.css') }}" type="text/css"> -->
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Invoice') }}
                </a>

            </div>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right" style="margin: 0px;">
                <!-- Authentication Links -->
                @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ URL::to('user/'.Auth::user()->id.'/changepassword') }}">
                                    Change Password
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endguest
            </ul>

            @guest
            <!-- Hide sidebar in login form-->
            @else
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav in" id="side-menu">
                        @if(Auth::user()->role == 'admin')
                            <li><a href="{{ route('user') }}"><span class="glyphicon glyphicon-user"></span> User</a></li>
                        @endif
                        <li>
                            <a href="{{ route('invoice.index') }}">
                                <span class="glyphicon glyphicon-list-alt"></span>
                                 Invoice
                            </a>
                        </li>
                        <li><a href="{{ route('item.index') }}"><span class="glyphicon glyphicon-apple"></span> Item</a></li>
                        <li><a href="{{ route('client.index') }}"><span class="glyphicon glyphicon-briefcase"></span> Client</a></li>
                    </ul>
                </div>
                @endguest
            </div>
            
        </nav>

        @yield('content')
    </div>
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- jQuery library -->
    <script src="{{ asset('js/jquery-1.12.4.js') }}"></script>

    <!-- Datatable -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('js/metisMenu.js') }}"></script>

    <!-- SB Admin -->
    <script src="{{ asset('js/sb-admin-2.js') }}"></script>
    
    @yield('script')
</body>
</html>

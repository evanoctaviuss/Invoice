<div class="row">
    <div class="jumbotron text-center">
        <h2>{{ $invoices->name }}</h2>
        <p>
            <strong>ID                  :</strong>{{ $invoices->id }}<br>
            <strong>Date                :</strong>{{ $invoices->invoice_date }}<br>
            <strong>Invoice Number      :</strong>{{ $invoices->invoice_no }}<br>
            <strong>Client Name         :</strong>{{ $invoices->client->business_name }}<br>

        </p>
    </div>

    <h1>Description</h1>
    <table class="table table-striped table-bordered" id="lines">
        <thead>
            <tr>
                <th>Item</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>

        @foreach($invoiceLines as $key => $value)
            <tr>
                <td><a href="{{ URL::to('item/' . $value->item_id) }}">{{ $value->item->name }}</a></td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->quantity }}</td>
                <td>{{ $value->item->price }}</td>
                <td>{{ $value->item->price * $value->quantity }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="container">
        <strong>Total       :</strong>{{ $invoices->total }}<br>
        <strong>Said     :</strong>{{ $invoices->in_words }}<br>
    </div>

</div>
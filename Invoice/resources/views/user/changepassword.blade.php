@extends('layouts.app')

@section('content')
<div style="width:auto; height:100%;" class="container" id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Form</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Change Password</div>

                <div class="panel-body">
                    @if ($errors->has('pass'))
                        <span class="help-block">
                            <strong style="color:#a94442;">{{ $errors->first('pass') }}</strong>
                        </span>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ URL::to('user/'.Auth::user()->id.'/changepassword/check') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                            <label for="old_password" class="col-md-4 control-label">Old Password</label>

                            <div class="col-md-6">
                                <input id="old_password" type="password" class="form-control" name="old_password" required>

                                @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                            <label for="new_password" class="col-md-4 control-label">New Password</label>

                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" required>

                                @if ($errors->has('new_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('new_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('conf_password') ? ' has-error' : '' }}">
                            <label for="conf_password" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="conf_password" type="password" class="form-control" name="conf_password" required>

                                @if ($errors->has('conf_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('conf_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-5">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                                <a href="{{ URL::to('invoice') }}" class="btn btn-primary">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

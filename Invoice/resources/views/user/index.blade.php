@extends('layouts.app')

@section('content')

<div style="width:auto; height:100%;" class="container" id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Showing all User</h1>
    </div>
</div>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="display responsive no-wrap table table-striped table-bordered" id="user_table" width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Last Login</th>
            <th>Option</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $key => $value)
        @if($value->role == 'user')
            <tr>
                <td>{{ $value->name }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ date('d-m-Y h:i:s', strtotime($value->last_login)) }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>

                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('user/' . $value->id) }}"><span class="glyphicon glyphicon-fullscreen"></span></a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('user/' . $value->id . '/edit') }}"><span class="glyphicon glyphicon-edit"></span></a>
                    <form class="del" method="POST" action="{{ URL::to('user/'.$value->id) }}" style="display:inline-block;">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn btn-danger">
                            <span class="glyphicon glyphicon-remove-sign"></span>
                        </button>
                    </form>

                </td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>

<nav class="navbar navbar-inverse pull-right" style="margin-top:20px;">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('/register') }}"><span class="glyphicon glyphicon-plus"></span> Register new User</a>
    </ul>
</nav>

</div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#user_table').DataTable();
    });
    $(".del").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
</script>
@endsection
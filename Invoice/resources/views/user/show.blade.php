@extends('layouts.app')

@section('content')

<div style="width:auto; height:100%;" class="container" id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Showing {{ $users->name }}</h1>
        </div>
    </div>

    <div class="jumbotron">
        <table cellpadding="5" cellspacing="5" width="100%">    
            <tr>         
                <td><strong>E-Mail     </strong></td>
                <td>:</td>
                <td>{{ $users->email }}</td>
            </tr>    
            <tr>         
                <td><strong>Last Login       </strong></td> 
                <td>:</td>
                <td>{{ $users->last_login }}</td>     
            </tr>
        </table>
    </div>
    <div class="pull-right">
        <a href="{{ URL::to('user') }}" class="btn btn-primary">
            Back
        </a>
    </div>

</div>
@endsection
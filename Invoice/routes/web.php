<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['middleware' => 'guest', function () {
    return view('auth.login');
}]);

Auth::routes();

Route::get('login', 'HomeController@index')->name('login')->middleware('guest');
Route::get('logout', 'Auth\LoginController@logout');

Route::get('user', 'UserController@index')->name('user')->middleware(['auth', 'admin']);
Route::get('user/{id}', 'UserController@show')->name('user.show')->middleware(['auth', 'admin']);
Route::get('user/{id}/edit', 'UserController@edit')->name('user.edit')->middleware(['auth', 'admin']);
Route::put('user/{id}', 'UserController@update')->name('user.update')->middleware(['auth', 'admin']);
Route::delete('user/{id}', 'UserController@destroy')->name('user.destroy')->middleware(['auth', 'admin']);
Route::get('user/{id}/changepassword', 'UserController@change')->name('user.changepass')->middleware('auth');
Route::put('user/{id}/changepassword/check', 'UserController@check')->middleware('auth');

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware(['auth','admin']);
Route::post('register', 'Auth\RegisterController@register')->middleware(['auth','admin']);

Route::resource('item', 'ItemController')->middleware('auth','role');
// Route::resource('invoice', 'InvoiceController')->middleware(['auth']);
Route::group(['prefix' => 'invoice'], function () {
    Route::get('','InvoiceController@index')->name('invoice.index')->middleware('auth');
    Route::get('create','InvoiceController@create')->name('invoice.create')->middleware('auth');
    Route::post('','InvoiceController@store')->name('invoice.store')->middleware('auth');
    Route::delete('/{id}','InvoiceController@destroy')->name('invoice.destroy')->middleware(['auth','role']);
    Route::put('/{id}','InvoiceController@update')->name('invoice.update')->middleware(['auth','role']);
    Route::get('/{id}','InvoiceController@show')->name('invoice.show')->middleware(['auth','role']);
    Route::get('/{id}/edit','InvoiceController@edit')->name('invoice.edit')->middleware(['auth','role']);
});

Route::get('/invoice/{id}/pdf','InvoiceController@viewPDF')->middleware('auth','role');

Route::post('/invoice/{id}/line','InvoiceLineController@store')->middleware('auth');
Route::get('/invoice/{id}/line/create','InvoiceLineController@create')->middleware('auth');
Route::delete('/invoice/{id}/line/{line_id}','InvoiceLineController@destroy')->name('invoice,line.delete')->middleware(['auth','role']);
Route::get('/invoice/{id}/line/{line_id}/edit','InvoiceLineController@edit')->name('invoice.line.edit')->middleware(['auth','role']);
Route::put('/invoice/{id}/line/{line_id}','InvoiceLineController@update')->middleware(['auth','role']);
// Route::resource('invoice_line', 'InvoiceLineController');

Route::resource('client', 'ClientController')->middleware('auth');